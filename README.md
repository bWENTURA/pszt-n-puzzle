# PSZT n-puzzle

## Opis

Napisać program rozwiązujący N-puzzle (N = n^2 – 1, gdzie n = {2, 3, ...} jest parametrem, eg. https://en.wikipedia.org/wiki/15_puzzle). Do przeszukiwania przestrzeni stanów należy wykorzystać algorytm BFS oraz algorytm A* lub IDA*. WE: liczba N do wygenerowania planszy lub plik z układem. WY: wynikowa sekwencja ruchów lub informacja że nie da się ułożyć.

## Wymagania

Python w wersji 3.8.0

## Uruchamianie

Aby uzyskać informacje na temat argumentów skryptu uruchom komendę:
```
python main.py -h
```
Aby uruchomić skrypt należy na wejściu podać z argumentem `-a` wykorzystywany algorytm (domyślnie `a_star_manhattan_distance`) a następnie podać argument:  
1. `-e` aby podać nazwę pliku przykładowego: z katalogu `examples/`
   ```
   python main.py -e <nazwa przykładu>
   ```
2. `-n` aby podać rozmiar losowo wygenerowanej tablicy do rozwiązania:
   ```
   python main.py -n <rozmiar n-puzzle>
   ```
Dodatkowo możliwe jest uruchomienie generatora wyników dla przebiegu wszystkich mozliwych algorytmów (bfs, A* Manhattan, A* Misplaced) dla kilku losowych tablic:
```
python generator_results.py -n <rozmiar n-puzzle> -s <liczba tablic do wygenerowania>
```
