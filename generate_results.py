import argparse
import json
import os
import datetime
from algorithms import a_star, bfs
from structures import State
from functions import functions

delimiter = "------------------------------------------------------------------------------------------------"

if __name__ == "__main__":
    examples_directory_name = "examples/"
    result_directory_name = "results/"
    if not os.path.exists(result_directory_name):
        os.mkdir(result_directory_name)

    argument_parser = argparse.ArgumentParser()
    argument_parser.add_argument(
        "-n",
        "--n_puzzle_size",
        help = "Specify size of n-puzzle problem.",
        action = "store",
        choices = range(3, 10),
        type = int,
        required = True
    )
    argument_parser.add_argument(
        "-s",
        "--samples_number",
        help = "Specify number of samples to run.",
        action = "store",
        choices = range(1, 10),
        type = int,
        required = True
    )
    args = argument_parser.parse_args() 

    for run_number in range(args.samples_number):
        time_stamp = datetime.datetime.now().strftime("%Y_%m_%d--%H-%M-%S")  
        n_size = args.n_puzzle_size
        board = functions.generate_n_puzzle(n_size)
        while not functions.check_if_solvable(board, n_size):
            board = functions.generate_n_puzzle(n_size)
        goal_board = functions.generate_goal_state(n_size)
        start_state = State.State(board, n_size, None, 0, None, None)

        print("Run number: " + str(run_number))
        print(start_state)

        print("Running A* (manhattan distance) algorithm...")
        path, time = a_star.a_star(start_state, goal_board, "a_star_manhattan_distance")

        result_file_name = result_directory_name + "result_a_star_manhattan_" + time_stamp + ".json"

        json_path = []
        for path_state in path:
            json_path.append(str(path_state).split("\n")[:-1])

        result_json = dict(
            size = n_size,
            algorithm = "a_star manhattan_distance",
            number_of_steps = len(path),
            algorithm_time = '{:.3f}'.format(time),
            path = json_path
        )

        with open(result_file_name, "w") as json_file:
            json.dump(result_json, json_file, indent = 4)
        
        print("Result of A* algorithm with manhattan distance heuristic in '" + str(result_file_name) + "' file.")

        print(delimiter)

        print("Running A* (misplaced_tiles) algorithm...")
        path, time = a_star.a_star(start_state, goal_board, "a_star_manhattan_distance")

        result_file_name = result_directory_name + "result_a_star_misplaced_tiles_" + time_stamp + ".json"

        json_path = []
        for path_state in path:
            json_path.append(str(path_state).split("\n")[:-1])

        result_json = dict(
            size = n_size,
            algorithm = "a_star misplaced_tiles",
            number_of_steps = len(path),
            algorithm_time = '{:.3f}'.format(time),
            path = json_path
        )

        with open(result_file_name, "w") as json_file:
            json.dump(result_json, json_file, indent = 4)
        
        print("Result of A* algorithm with misplaced_tiles heuristic in '" + str(result_file_name) + "' file.")

        print(delimiter)

        print("Running BFS algorithm...")
        path, time = bfs.bfs(start_state, goal_board)

        result_file_name = result_directory_name + "result_bfs_" + time_stamp + ".json"

        json_path = []
        for path_state in path:
            json_path.append(str(path_state).split("\n")[:-1])

        result_json = dict(
            size = n_size,
            algorithm = "bfs",
            number_of_steps = len(path),
            algorithm_time = '{:.3f}'.format(time),
            path = json_path
        )

        with open(result_file_name, "w") as json_file:
            json.dump(result_json, json_file, indent = 4)

        print("Result of BFS algorithm in '" + str(result_file_name) + "' file.")
        
        if time < 300:
            example_json = dict(
                size = n_size,
                puzzle_board = json_path[0]
            )
            example_file_name = functions.save_example_to_file(example_json, examples_directory_name)
            print("Example saved to '" + str(example_file_name) + "' file.")

        print(delimiter)


