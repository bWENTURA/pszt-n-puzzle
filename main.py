import argparse
import json
import os
import datetime
from algorithms import a_star, bfs
from structures import State
from functions import functions

if __name__ == "__main__":
    examples_directory_name = "examples/"
    examples_names = functions.get_examples_names(examples_directory_name)

    result_directory_name = "results/"
    if not os.path.exists(result_directory_name):
        os.mkdir(result_directory_name)

    argument_parser = argparse.ArgumentParser()
    argument_parser.add_argument(
        "-n",
        "--n_puzzle_size",
        help = "Specify size of n-puzzle problem.",
        action = "store",
        choices = range(3, 10),
        type = int
    )
    argument_parser.add_argument(
        "-a",
        "--algorithm",
        help = "Specify algorithm type for solving n-puzzle.",
        action = "store",
        choices = ["bfs", "a_star_manhattan_distance", "a_star_misplaced_tiles"],
        default = "a_star_manhattan_distance"
    )
    argument_parser.add_argument(
        "-e",
        "--example_name",
        help = "Specify name of example you want to run.",
        action = "store",
        choices = examples_names,
    )
    args = argument_parser.parse_args() 

    if args.example_name and args.n_puzzle_size:
        argument_parser.error("Don't provide -n and -e arguments in the same time.")
    else:
        n_size = None

        if args.example_name:
            example_path = examples_directory_name + args.example_name
            n_size, board = functions.read_input(example_path)
        elif args.n_puzzle_size:
            n_size = args.n_puzzle_size
            board = functions.generate_n_puzzle(n_size)
        else:
            argument_parser.error("Provide -n argument for size of generation or -e argument with name of example json from 'examples/'.")

        start_state = State.State(board, n_size, None, 0, None, None)

        print("Board to solve:")
        print(start_state)

        if functions.check_if_solvable(board, n_size):
            goal_board = functions.generate_goal_state(n_size)

            time_stamp = datetime.datetime.now().strftime("%Y_%m_%d--%H-%M-%S")

            result_file_name = result_directory_name + "result_" + args.algorithm + "_" + time_stamp + ".json"

            if args.algorithm == "bfs":
                print("Running BFS algorithm...")
                path, time = bfs.bfs(start_state, goal_board)
            else:
                heuristic_name = "Misplaced Tiles"

                if args.algorithm == "a_star_manhattan_distance":
                    result_file_name = result_directory_name + "result_" + args.algorithm + "_" + time_stamp + ".json"

                    heuristic_name = "Manhattan Distance"
                
                print("Running A* (" + str(heuristic_name) + ") algorithm...")
                path, time = a_star.a_star(start_state, goal_board, args.algorithm)

            json_path = []
            for path_state in path:
                json_path.append(str(path_state).split("\n")[:-1])

            result_json = dict(
                size = n_size,
                algorithm = args.algorithm,
                number_of_steps = len(path),
                algorithm_time = '{:.3f}'.format(time),
                path = json_path
            )

            with open(result_file_name, "w") as json_file:
                json.dump(result_json, json_file, indent = 4)
            
            print("Result of algorithm in '" + str(result_file_name) + "' file.")

        else:
            print("Generated puzzle board is not solvable.")