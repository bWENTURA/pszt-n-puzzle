import random
import json
import os

def read_input(example_path):
    """
    Function reading input board and board size from json file.

    Parameters:
    example_path    -- path of example file, relative to main script

    Returns:
    puzzle_size     -- integer, n size of n-puzzle board 
    puzzle_board    -- list of list representing board read from file
    """
    puzzle_size = None
    puzzle_board = []

    with open(example_path, "r") as input_file:
        data = json.load(input_file)

    puzzle_size = int(data["size"])
    board_str = data["puzzle_board"]

    for row in board_str:
        row = row[1:-1]
        row_of_int = [int(x) for x in row.split()]
        puzzle_board.append(row_of_int)

    return puzzle_size, puzzle_board


def check_if_solvable(puzzle_board, puzzle_size):
    """
    Function which checks if board is solvable.

    Parameters:
    puzzle_board    -- list of list representing board
    puzzle_size     -- size of puzzle_board

    Returns:
    Boolean value
    """
    state_list = []
    row_with_blank_space = puzzle_size
    for index, puzzle_row in enumerate(puzzle_board):
        state_list += puzzle_row
        if 0 in puzzle_row:
            row_with_blank_space -= index


    inversion_sum = 0
    for index in range(1, len(state_list)):
        number = state_list[index]
        elements_to_compare = state_list[:index]
        if number:
            inversion_sum += sum(element > number for element in elements_to_compare)

    if puzzle_size % 2:
        if not inversion_sum % 2:
            return True        
    else:
        if inversion_sum % 2:
            if not row_with_blank_space % 2:
                return True
        else:
            if row_with_blank_space % 2:
                return True
    return False

def generate_n_puzzle(n_size):
    """
    Function which generates random n-puzzle board.

    Parameters:
    n_size  --  integer, representing n-size of n-puzzle

    Returns:
    board   --   list of list representing n-puzzle board
    """
    puzzle_list = [number for number in range(n_size * n_size)]
    random.shuffle(puzzle_list)
    board = []

    for index in range(1, n_size + 1):
        start = (index - 1) * n_size 
        end = start + n_size
        board.append(puzzle_list[start:end])

    return board

def generate_goal_state(n_size):
    """
    Function which generates goal for n-puzzle problem.

    Parameters:
    n_size  --  integer, representing n-size of n-puzzle

    Returns:
    board   --   list of list representing n-puzzle board
    """
    goal_state_list = [number for number in range(1, n_size * n_size)]
    goal_state_list.append(0)
    goal_state = []

    for index in range(1, n_size + 1):
        start = (index - 1) * n_size 
        end = start + n_size
        goal_state.append(goal_state_list[start:end])

    return goal_state

def get_examples_names(examples_directory_name):
    """
    Searches for names of examples in given directory.

    Parameters:
    examples_directory_name --  path of directory in which we want to look for examples,
                                relative to main script

    Returns:
    examples_names          --  list of examples names
    """
    examples_names = []
    if not os.path.exists(examples_directory_name):
        os.mkdir(examples_directory_name)
    else:
        examples_names = os.listdir(examples_directory_name)

    return examples_names

def save_example_to_file(example_json, examples_directory_name):
    """
    Saves json dictionary in next possible example_*.json file.

    Parameters:
    example_json            --  dictionary containing information about example
    examples_directory_name --  path of directory in which we want to save example,
                                relative to main script

    Returns:
    example_file_name       --  name of file where json has been saved
    """
    examples_names = get_examples_names(examples_directory_name)

    example_number = 0
    example_file_name = "example_" + str(example_number) + ".json"

    while example_file_name in examples_names:
        example_number += 1
        example_file_name = '_'.join(example_file_name.split("_")[:-1]) + "_" + str(example_number) + ".json"
    
    example_file_name = examples_directory_name + example_file_name

    with open(example_file_name, "w") as json_file:
        json.dump(example_json, json_file, indent = 4)

    return example_file_name

    

