import time
from structures import State

def a_star(start_state, goal, heuristic_type):
    """
    a_star function implements behaviour of A* algorithm.
    For heuristic function it uses Manhattan Distance.

    Parameters:
    start_state     --  state object representing start state
    goal            --  2D matrix (list of lists) representing goal to which we want build the path
    heuristic_type  --  string with name of heuristic used when calculating f score

    Returns:
    path            --  list of states which represents path from start state to goal
    elapsed_time    --  time of algorithm measured using time standard module
    """
    manhattan = False
    if heuristic_type == "a_star_manhattan_distance":
        manhattan = True

    start_time = time.time()

    candidates = [(start_state, start_state.manhattan_distance(goal))]
    closed_set = []

    while candidates:
        current_candidate = candidates[0]
        candidates = candidates[1:]

        if current_candidate[0] == goal:
            closed_set.append(current_candidate)
            break
        else:
            if manhattan:
                candidate_children = [(children, current_candidate[0].depth + children.manhattan_distance(goal)) for children in current_candidate[0].expand()]
            else:
                candidate_children = [(children, current_candidate[0].depth + children.num_of_misplaced_tiles(goal)) for children in current_candidate[0].expand()]

            for children in candidate_children:
                if children[0] in closed_set:
                   continue

                candidate_index = None
                for index, element in enumerate(candidates):
                    if element[0] == children[0]:
                        candidate_index = index
                        break

                if candidate_index:
                    if candidates[candidate_index][1] > children[1]:
                        candidates[candidate_index] = children
                        continue
                
                candidates.append(children)
        
        candidates.sort(key = lambda candidate: candidate[1])
        closed_set.append(current_candidate)
    
    end_time = time.time()
    elapsed_time = end_time - start_time

    next_state = closed_set[-1][0]
    path = [next_state]
    while next_state != start_state:
        next_state = next_state.parent
        path.append(next_state)

    path.reverse()
    return path, elapsed_time