from structures import State
import time

def bfs(start_state, goal):
    """
    bfs function implements behaviour of BFS algorithm.

    Parameters:
    state           --  state object representing start state
    goal            --  2D matrix (list of lists) representing goal to which we want build the path

    Returns:
    path            --  list of states which represents path from start state to goal
    elapsed_time    --  time of algorithm measured using time standard module
    """

    if start_state == goal:
        return [start_state], 0

    start = time.time()
    explored = []
    queue = [start_state]
    path = []
    
    while queue:
        current = queue.pop(0)
        children = current.expand()
        for state in children:
            if state not in queue+explored:
                queue.append(state)
        explored.append(current)
        if current == goal:
            while current != start_state:
                path.append(current)
                current = current.parent
            path.append(current)
            break
        
    end = time.time()
    elapsed_time = end - start
    path.reverse()
    return path, elapsed_time
