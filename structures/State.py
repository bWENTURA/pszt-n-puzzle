import copy

class State:

    def __init__(self, board, size, parent, depth, zero_pos, action):
        """
        Parameters:
        board       -- 2D matrix (list of lists) representing puzzle board
        size        -- size of board
        parent      -- parent state
        depth       -- represents depth of state
        zero_pos    -- tuple representing coordinates of zero in board
        action      -- string representing action taken to get to this state from parent state
        """
        self.board = board
        self.size = size
        self.parent = parent
        self.depth = depth
        self.zero_pos = zero_pos
        self.action = action

        self.set_zero_pos()

    def __eq__(self, other):
        """
        Parameters:
        other       -- state object or list of lists representing n-puzzle board

        Returns:
        Boolean value of comparison of state and other state/board
        """
        if isinstance(other, self.__class__):
            other_board = other.board
        elif isinstance(other, self.board.__class__):
            other_board = other
        else: return False
        return self.board == other_board

    def __str__(self):
        """
        Returns:
        board_str   --  representation of state as properly formatted string with state's information
        """
        lenght = str(len(str(self.size * self.size)) + 2)
        board_str = ''
        for row in self.board:
            board_str += '['
            for val in row:
                board_str += ('{:' + lenght + '}').format(val)
            board_str += ']\n'
        return board_str

    def __repr__(self):
        """
        ?
        """
        repr = '\nBoard : \n{}depth: {} \nzero_pos: {} \naction: {}'.format(str(self), self.depth, self.zero_pos, self.action)
        return repr

    def locate_tile(self, value):
        """
        Parameters:
        value   -- value which we want to find in board
        
        Returns:
        value   -- tuple of coordinates of given value
        """
        for y, row in enumerate(self.board):
            for x, elem in enumerate(row):
                if elem == value:
                    return (x, y)
        return None

    def set_zero_pos(self):
        """
        Method finds position of 0 in board and sets object variable
        """
        if self.zero_pos is None:
            self.zero_pos = self.locate_tile(0)

    def manhattan_distance(self, goal):
        """
        Method calculating manhattan distance in comparison to goal.

        Parameters:
        goal    -- goal to which we have to compare state object to find Manhattan Distance

        Returns:
        dist    -- integer, value of Manhattan Distance
        """
        dist = 0
        for i, row in enumerate(goal):
            for j, elem in enumerate(row):
                finish = self.locate_tile(elem)
                start = (j, i)
                dist += sum(abs(a-b) for a, b in zip(start, finish))
        return dist

    def num_of_misplaced_tiles(self, goal):
        """
        Method calculating sum of misplaced tiles in comparison to goal.

        Parameters:
        goal    -- goal to which we have to compare state object to find Manhattan Distance

        Returns:
        num     -- integer, value of Misplaced Tiles
        """
        num = 0
        for i, row in enumerate(self.board):
            for j, elem in enumerate(row):
                if elem != goal[i][j]:
                    num += 1
        return num

    def expand(self):
        """
        Method finding states to which we can proceed by moving 0 tile.
        Returns:
        children    -- list of state objects representing possible moves
        """
        children = []
        x = self.zero_pos[0]
        y = self.zero_pos[1]

        # move up
        if y > 0 and self.action != 'down':
            new_board = copy.deepcopy(self.board)
            new_board[y][x], new_board[y-1][x] = new_board[y-1][x], new_board[y][x]
            children.append(State(new_board, self.size, self, self.depth + 1, (x, y-1), 'up'))
        # move down
        if y < len(self.board)-1 and self.action != 'up':
            new_board = copy.deepcopy(self.board)
            new_board[y][x], new_board[y+1][x] = new_board[y+1][x], new_board[y][x]
            children.append(State(new_board, self.size, self, self.depth + 1, (x, y+1), 'down'))
        # move left
        if x > 0 and self.action != 'right':
            new_board = copy.deepcopy(self.board)
            new_board[y][x], new_board[y][x-1] = new_board[y][x-1], new_board[y][x]
            children.append(State(new_board, self.size, self, self.depth + 1, (x-1, y), 'left'))
        #move right
        if x < len(self.board)-1 and self.action != 'left':
            new_board = copy.deepcopy(self.board)
            new_board[y][x], new_board[y][x+1] = new_board[y][x+1], new_board[y][x]
            children.append(State(new_board, self.size, self, self.depth + 1, (x+1, y), 'right'))
        return children
