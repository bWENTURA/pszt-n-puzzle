from structures import State
from algorithms import a_star
from functions import functions
import unittest

class AStarTest(unittest.TestCase):
    
    def test_a_star_manhattan(self):
        board = [[1, 2, 3], [6, 4, 5], [0, 7, 8]]
        s0 = State.State(board, len(board), None, 0, None, None)
        goal = functions.generate_goal_state(3)
        result, time = a_star.a_star(s0, goal, "manhattan_distance")
        for i, state in enumerate(result):
            print('Step {} - {}'.format(i, state.action))
            print(state)
        print('time elapsed: {:.3f} s'.format(time))

    def test_a_star_misplaced(self):
        board = [[1, 2, 3], [6, 4, 5], [0, 7, 8]]
        s0 = State.State(board, len(board), None, 0, None, None)
        goal = functions.generate_goal_state(3)
        result, time = a_star.a_star(s0, goal, "a_star_misplaced_tiles")
        for i, state in enumerate(result):
            print('Step {} - {}'.format(i, state.action))
            print(state)
        print('time elapsed: {:.3f} s'.format(time))

if __name__=="__main__":
    unittest.main()