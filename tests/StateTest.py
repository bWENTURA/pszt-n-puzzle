from structures import State
import unittest

class StateTest(unittest.TestCase):

    def test_eq(self):
        board0 = [[1, 5, 6], 
                    [2, 3, 8], 
                    [5, 0, ]]
        board1 = [[1, 4, 6], 
                    [2, 3, 8], 
                    [5, 0, ]]
        board2 = [[1, 5, 6],
                    [2, 3, 4],
                    [5, 0, 1]]
        board3 = [[1, 2], [2, 3], [1, 2]]
        s0 = State.State(board0, 3, None, 1, (1, 2), None)
        s1 = State.State(board0, 3, s0, 2, (1, 1), None)
        s2 = State.State(board1, 3, s1, 3, None, 'right')
        self.assertEqual(s0, s1, 'State : Incorrect comparison')
        self.assertNotEqual(s0, s2, 'State : Incorrect comparison')
        self.assertEqual(s0, board0, 'State and 2D list : Incorrect comparison')
        self.assertNotEqual(s0, board2, 'State and 2D list : Incorrect comparison')
        self.assertNotEqual(s0, board3, 'State and wrong sized list : Incorrect comparison')

    def test_expand(self):
        board0 = [[1, 5, 6], 
                    [2, 3, 8], 
                    [5, 0, 7]]
        s0 = State.State(board0, 3, None, 0, None, None)
        s0.set_zero_pos()
        children = s0.expand()
        ex0 = [[1, 5, 6], 
                    [2, 0, 8], 
                    [5, 3, 7]]
        ex1 = [[1, 5, 6], 
                    [2, 3, 8], 
                    [0, 5, 7]]
        ex2 = [[1, 5, 6], 
                    [2, 3, 8], 
                    [5, 7, 0]]
        not_ex = [[0, 5, 6], 
                    [2, 3, 8], 
                    [5, 7, 1]]
        self.assertIn(ex0, children)
        self.assertIn(ex1, children)
        self.assertIn(ex2, children)
        self.assertNotIn(not_ex, children)

    def test_man_distance(self):
        board0 = [[1, 5, 6], 
                    [2, 3, 8], 
                    [4, 0, 7]]
        s0 = State.State(board0, 3, None, 0, None, None)
        goal = [[1, 2, 3],
                [4, 5, 6],
                [7, 8, 0]]
        dist = s0.manhattan_distance(goal)
        actual_dist = 2+2+1+1+1+2+2+1
        self.assertEqual(dist, actual_dist, 'Incorrect Manhattan Distance')

    def test_misplaced_tiles(self):
        board0 = [[1, 5, 6], 
                    [2, 3, 8], 
                    [4, 0, 7]]
        s0 = State.State(board0, 3, None, 0, None, None)
        goal = [[1, 2, 3],
                [4, 5, 6],
                [7, 8, 0]]
        misplaced = s0.num_of_misplaced_tiles(goal)
        self.assertEqual(misplaced, 8, 'Incorrect Number of misplaced tiles')



if __name__ == '__main__':
    unittest.main()