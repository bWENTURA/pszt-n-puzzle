import unittest
from functions import functions

class FunctionTest(unittest.TestCase):
    
    def test_goal_state_generator(self):
        board = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 0]]
        goal = functions.generate_goal_state(4)
        self.assertEqual(board, goal, 'Goal state generated incorrectly')

if __name__ == '__main__':
    unittest.main() 