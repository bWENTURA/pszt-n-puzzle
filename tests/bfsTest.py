from structures import State
from algorithms import bfs
from functions import functions
import unittest

class bfsTest(unittest.TestCase):
    
    def test_bfs(self):
        board0 = [[1, 0, 2, 4], [5, 7, 3, 8], [9, 6, 11, 12], [13, 10, 14, 15]]
        s0 = State.State(board0, 4, None, 0, None, None)
        s0.set_zero_pos()
        goal = functions.generate_goal_state(4)
        result, time = bfs.bfs(s0, goal)
        for i, state in enumerate(result):
            print('Step {} - {}'.format(i, state.action))
            print(state)
        print('time elapsed: {:.3f} s'.format(time))
        return

if __name__ == '__main__':
    # unittest.main()
    test = bfsTest()
    test.test_bfs()